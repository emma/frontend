import "@/styles/404.scss";
import onLoad from "./onload.js";
import sandalsVoice from "@/audio/sandals.webm";
import jokes from "./jokes.json";
console.log(jokes);

onLoad(function() {
  const humorLink = document.getElementById("humor-link");
  const sandalsMessage = document.getElementById("sandals-message");
  const sandals = document.getElementById("sandals");
  const waitingOnPlay = [];
  let humorous = false;
  humorLink.addEventListener("click", function() {
    document.getElementById("humor").style.display = "block";
    document.getElementById("no-humor").style.display = "none";
    const awaitEnter = () =>
      new Promise(resolve =>
        window.addEventListener("keypress", ev => {
          if (ev.which == 13) resolve();
        })
      );

    awaitEnter().then(async () => {
      const sandalsNoises = await Promise.all(
        // We overlay them so we keep a store of 10
        new Array(10).fill(null).map(async () => {
          const audio = new Audio(sandalsVoice);
          await audio.load();
          return audio;
        })
      );
      let lastNoise = 0;
      async function type(text) {
        sandalsMessage.textContent = "* ";
        for (const character of text) {
          // if (character.match(/[a-z]/i)) {
          const noiseId = lastNoise++;
          if (lastNoise >= sandalsNoises.length) lastNoise = 0;
          await sandalsNoises[noiseId]
            .play()
            .catch(err => console.warn("Error with sandals voice", err));

          await new Promise(resolve => setTimeout(resolve, 70));
          sandalsMessage.textContent += character;
        }
      }
      await awaitEnter();
      sandals.style.display = "block";
      await type("404! not found!");
      await new Promise(resolve =>
        window.addEventListener("keypress", ev => {
          if (ev.which == 13) resolve();
        })
      );
      await type(
        `Sorry ${(client.profile && client.profile.username) || "kid"}`
      );

      let typing = false;
      window.addEventListener("keypress", ev => {
        if (ev.which == 13 && !typing) {
          typing = true;
          const joke = jokes[Math.floor(Math.random() * jokes.length)];
          type("Want to hear a joke?")
            .then(awaitEnter)
            .then(() => type(joke[0]))
            .then(awaitEnter)
            .then(() => type(joke[1]))
            .then(() => (typing = false));
        }
      });
    });
    for (const object of objects) {
      rects[object.id] = object.getBoundingClientRect();
      oldTransforms[object.id] = object.attributes.transform.value;
    }
    humorous = true;

    for (const cb of waitingOnPlay) {
      console.log("Humorlink clicked, starting the player");
      cb();
    }
  });
  const objects = [
    "letter_s",
    "letter_o",
    "letter_r",
    "letter_r2",
    "letter_y"
  ].map(o => document.getElementById(o));
  const rects = {};
  const oldTransforms = {};

  let timeout = null;
  document.addEventListener("mousemove", function mouseHandle(e) {
    if (!humorous) return;
    if (timeout) clearTimeout(timeout);
    timeout = setTimeout(function() {
      for (const object of objects) {
        object.classList = "";
      }
    }, 500);
    for (const object of objects) {
      object.classList = "not-bobbing";
      const rect = rects[object.id];
      const parentX = 0;
      const parentY = 0;
      let deltaX = e.clientX - parentX - object.clientWidth / 2;
      let deltaY = e.clientY - parentY - object.clientHeight / 2;
      const mouseX = deltaX;
      const mouseY = deltaY;
      const divisor = (Math.abs(rect.x - mouseX) || 1) / 2;
      // console.log("WHat is divisor", divisor);
      let targetY = mouseY - 100 - divisor;
      targetY = targetY / 20;
      // console.log("IWI", targetY, document.body.clientHeight);
      const newTransform = `translate(0 ${Math.round(targetY)}) ${
        oldTransforms[object.id]
      }`;
      object.setAttribute("transform", newTransform);
    }
  });
  const momos = [
    {
      e: document.getElementById("momo-1"),
      i: 0,
      f: 0,
      s: 1
    },
    {
      e: document.getElementById("momo-2"),
      i: 0,
      f: 0,
      s: 1.2
    },
    {
      e: document.getElementById("momo-3"),
      i: 0,
      f: 0,
      s: 0.8
    },
    {
      e: document.getElementById("momo-4"),
      i: 0,
      f: 0,
      s: 1.2
    },
    {
      e: document.getElementById("momo-5"),
      i: 0,
      f: 0,
      s: 0.9
    },
    {
      e: document.getElementById("momo-6"),
      i: 0,
      f: 0,
      s: 0.8
    }
  ];
  let i = 0;
  const directions = [
    {
      pos: "left",
      rot: 0
    },
    {
      pos: "right",
      rot: 180
    },
    {
      pos: "top",
      rot: 90,
      c: true
    },
    {
      pos: "bottom",
      rot: 270,
      c: true
    }
  ];
  (async () => {
    if (
      navigator.userAgent.match(
        /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini|Mobile|mobile|CriOS/
      ) ||
      location.search == "?nowebm"
    ) {
      const buffs = await import("@/js/duckymomo.js").then(m => m.default);
      const frames = buffs.map((
        r //r
      ) => URL.createObjectURL(new Blob([new Uint8Array(r)])));

      setInterval(() => {
        for (const momo of momos) {
          momo.i = (i / momo.s) % 74;
          if (frames[momo.i]) momo.e.src = frames[momo.i];
          else momo.e.display = "none";
          if (momo.i == 0) {
            moveMomo(momo);
          }
        }
        i++;
      }, 1000 / 25);
    } else {
      const video = await import("@/images/momo/momo.webm");
      for (const momo of momos) {
        momo.e.display = "none";
        const videoElement = document.createElement("video");
        videoElement.muted = true;
        videoElement.style.display = "none";
        videoElement.classList = "momo";
        videoElement.src = video.default;
        videoElement.load();
        momo.e.parentElement.appendChild(videoElement);
        videoElement.id = momo.e.id;
        momo.e.remove();
        momo.e = videoElement;
        videoElement.playbackSpeed = momo.s;
        // videoElement.controls = true;
        // function tickFrame() {
        //   videoElement.play();
        //   setTimeout(() => {
        //     videoElement.pause();
        //     moveMomo(momo);
        //     setTimeout(() => {
        //       tickFrame();
        //     }, 1000 / 24 * 29 * momo.s);
        //   }, 1000 / 25 * 45 * momo.s);
        // }

        videoElement.addEventListener("timeupdate", function(ev) {
          // console.log(ev);
          if (videoElement.currentTime >= videoElement.duration) {
            videoElement.pause();
            videoElement.position = 0;
            moveMomo(momo);
            setTimeout(
              () => videoElement.play().catch(err => console.warn(err)),
              1000 / 24 * 29 * momo.s
            );
          }
        });
        function onMomoPlay() {
          console.log("Momo video is playing...", new Error("dspacit"));
          moveMomo(momo);
          videoElement.style.display = "";
        }

        function playMomoVideo() {
          console.log("playMomoVideo called");
          const mightBePromise = videoElement.play();
          if (mightBePromise && mightBePromise.then) {
            mightBePromise
              .then(function() {
                console.log("Promise resolved, onMomoPlay calling");
                onMomoPlay();
              })
              .catch(function(err) {
                console.log(
                  "Caught an error while trying to play... Probably an autoplay complaint!",
                  err
                );
                waitingOnPlay.push(playMomoVideo);
              });
          } else onMomoPlay();
        }
        videoElement.addEventListener("canplaythrough", playMomoVideo);
      }
    }
    function moveMomo(momo) {
      momo.e.display = "";
      const dir = directions[Math.floor(Math.random() * directions.length)];

      momo.e.style.transform = `rotate(${dir.rot}deg)`;
      if (dir.c) {
        momo.e.style.left = Math.floor(
          Math.random() * (document.body.clientWidth - momo.e.style.clientWidth)
        );
        momo.e.style.right = 0;
        momo.e.style.top = "";
        momo.e.style.bottom = "";
      } else {
        momo.e.style.top = Math.floor(
          Math.random() *
            (document.body.clientHeight - momo.e.style.clientWidth)
        );
        momo.e.style.bottom = 0;
        momo.e.style.left = "";
        momo.e.style.right = "";
      }
      if (dir.c) momo.e.style[dir.pos] = "10px";
      else momo.e.style[dir.pos] = 0;
    }
  })();
});
