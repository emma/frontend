import wrapPromise from "./wrapPromise.js";
import common from "./commonCode.js";
import onLoad from "./onload.js";
import "@/styles/account.scss";
import "@/styles/forms.scss";

const domainPromise = wrapPromise(() => client.getDomains());

onLoad(async function() {
  let errorBox = null;
  const paranoid = document.getElementById("paranoid");
  const updateAccountBtn = document.getElementById("submit-btn");
  const domainSelector = document.getElementById("domain-selector");
  const shortenDomainSelector = document.getElementById("s-domain-selector");
  const wildcard = document.getElementById("wildcard");
  const shortenWildcard = document.getElementById("s-wildcard");
  const tokenPassword = document.getElementById("token-password");
  const generateTokenBtn = document.getElementById("generate-token");

  // Submit token modal on enter
  tokenPassword.addEventListener("keydown", function(ev) {
    if (ev.key == "Enter") {
      createToken();
    }
  });

  // Disable form submit events
  const passwordForm = document.getElementById("password-form");
  passwordForm.addEventListener("submit", function(ev) {
    ev.preventDefault();
    ev.stopPropagation();
    return false;
  });
  const updateForm = document.getElementById("update-form");
  updateForm.addEventListener("submit", function(ev) {
    ev.preventDefault();
    ev.stopPropagation();
    return false;
  });

  await window.profilePromise;

  let paranoidVal = client.profile.paranoid;
  paranoid.value = paranoidVal.toString();

  wildcard.value = window.client.profile.subdomain || "";
  shortenWildcard.value =
    (client.profile.shorten_domain
      ? client.profile.shorten_subdomain
      : client.profile.subdomain) || "";

  const { domains, officialDomains } = await domainPromise;
  console.log("Fetched domains:", domains);
  const options = Object.entries(domains).map(
    ([id, domain]) => new Option(domain.replace("*.", ""), id, false, false)
  );

  for (const option of options) {
    domainSelector.appendChild(option);
    shortenDomainSelector.appendChild(option.cloneNode(true));
  }
  domainSelector.value = window.client.profile.domain;
  shortenDomainSelector.value =
    client.profile.shorten_domain || client.profile.domain;

  function checkShortenDomainSelector() {
    if (domains[shortenDomainSelector.value].startsWith("*.")) {
      shortenDomainSelector.parentNode.classList = "input-group show-wildcard";
    } else {
      shortenDomainSelector.parentNode.classList = "input-group";
    }
  }

  function checkDomainSelector() {
    if (domains[domainSelector.value].startsWith("*.")) {
      domainSelector.parentNode.classList = "input-group show-wildcard";
    } else {
      domainSelector.parentNode.classList = "input-group";
    }
  }
  try {
    checkDomainSelector();
    checkShortenDomainSelector();
  } catch (err) {}

  // This would be an addEventListener but jQuery is garbage
  domainSelector.onchange = checkDomainSelector;
  shortenDomainSelector.onchange = checkShortenDomainSelector;

  let domainId = client.profile.domain;
  let wildcardVal = client.profile.subdomain;
  let shortenDomainId = isNaN(client.profile.shorten_domain)
    ? domainId
    : client.profile.shorten_domain;
  let shortenWildcardVal = client.profile.shorten_subdomain;

  async function applyChanges() {
    common.handleErr();
    common.removeAlert(errorBox);
    const modifications = {};

    if (shortenDomainSelector.value != shortenDomainId) {
      modifications.shorten_domain = Number(shortenDomainSelector.value);
    }
    if (
      shortenWildcard.value != shortenWildcardVal &&
      domains[shortenDomainSelector.value].startsWith("*.")
    )
      modifications.shorten_subdomain = shortenWildcard.value;

    if (domainSelector.value != domainId) {
      modifications.domain = Number(domainSelector.value);
    }
    if (
      wildcard.value != wildcardVal &&
      domains[domainSelector.value].startsWith("*.")
    )
      modifications.subdomain = wildcard.value;

    if ((paranoid.value == "true") != paranoidVal) {
      modifications.paranoid = paranoid.value == "true";
    }

    if (Object.keys(modifications).length) {
      try {
        await client.updateAccount(modifications);
        errorBox = common.sendAlert("success", "Your changes have been saved!");

        if (modifications.domain) domainId = modifications.domain;
        if (modifications.subdomain) wildcardVal = modifications.subdomain;
        if (modifications.shorten_domain)
          shortenDomainId = modifications.domain;
        if (modifications.shorten_subdomain)
          shortenWildcardVal = modifications.subdomain;
        if (modifications.paranoid !== undefined)
          paranoidVal = modifications.paranoid;
      } catch (err) {
        common.handleErr(err);
      }
    }
  }

  updateAccountBtn.addEventListener("click", applyChanges);

  generateTokenBtn.addEventListener("click", function() {
    createToken();
  });

  async function createToken() {
    try {
      const token = await client.generateToken(tokenPassword.value);
      window.location.hash = token;
      window.location.pathname = "/token.html";
    } catch (err) {
      tokenPassword.setCustomValidity(err.userMessage || err.message);
      passwordForm.classList = "was-validated needs-validation";
      tokenPassword.value = "";
      tokenPassword.focus();
      return;
    }
  }
  console.log("reached end");
});
