import Client from "./Client.js";
import config from "../../config.json";

window.client = new Client({
  endpoint: config.ENDPOINT_OVERRIDE || `${window.location.origin}/api`,
  token: window.localStorage.getItem("token")
});

window.profilePromise = window.client
  .getProfile()
  .then(r => r)
  .catch(err => {
    if(err.message == "BAD_AUTH") {
        localStorage.setItem("token", "");
        localStorage.removeItem("token");
        window.client.token = null;
    }
  });

const loadEvent = new Event("clientLoad");
window.dispatchEvent(loadEvent);
