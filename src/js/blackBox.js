const blackBox = {
  dump() {
    const link = document.createElement("a");
    const filename = "elixire-blackbox-log.txt";
    // Everything we need supports Blobs sooo :shrugz:
    const blob = new Blob([localStorage.blackBox], { type: "text/plain" });
    if (navigator.msSaveBlob) {
      link.addEventListener("click", function() {
        navigator.msSaveBlob(blob, filename);
      });
    } else {
      link.href = URL.createObjectURL(blob);
      link.download = filename;
    }
    link.click();
  },
  get enabled() {
    return (
      localStorage.blackBox !== "undefined" &&
      localStorage.blackBox !== undefined
    );
  },
  enable() {
    console.log("Blackbox is enabled!");
    alert(
      "Blackbox is recording! Be careful leaving this on for long! Disable it on the footer!"
    );
    blackBox.log("Blackbox enabled");
    const lsClean = JSON.parse(JSON.stringify(localStorage));
    delete lsClean.blackBox;
    blackBox.log(`Current localStorage: ${JSON.stringify(lsClean)}`);
    console._log = console.log;
    console.log = function(...args) {
      blackBox.log(`Logged to console: ${JSON.stringify(args)}`);
      console._log(...args);
    };
    // Object.defineProperty(window, "location", {
    //   set(newValue) {
    //     blackBox.log(`window.location updated: ${JSON.stringify(newValue)}`);
    //     window.location = newValue;
    //   },
    //   get() {
    //     return window.location;
    //   }
    // });
    window.addEventListener("click", function(ev) {
      blackBox.log(
        `Registered click on element ${ev.path.map(elem =>
          blackBox.inspectElement(elem)
        )} @ ${ev.pageX},${ev.pageY}`
      );
    });
    window.addEventListener("keydown", function(ev) {
      blackBox.log(
        `Keydown: ${ev.code}, element: ${ev.path.map(elem =>
          blackBox.inspectElement(elem)
        )}, current element value: ${ev.value}`
      );
    });
    window.addEventListener("keyup", function(ev) {
      blackBox.log(
        `Keyup: ${ev.code}, element: ${ev.path.map(elem =>
          blackBox.inspectElement(elem)
        )}, current element value: ${ev.value}`
      );
    });
    window.addEventListener("beforeunload", function() {
      blackBox.log(`beforeunload fired on ${location.toString()}`);
    });
  },
  inspectElement(element) {
    return `${element.tagName}[class='${element.className}',id='${
      element.id
    }']`;
  },

  clear() {
    localStorage.blackBox = "";
    blackBox.log("Cleared blackbox!");
  },

  log(data) {
    if (!blackBox.enabled) return;
    try {
      localStorage.blackBox += `{{{ [${location.toString()}] [${new Date().getTime()}] ${data} #${
        new Error("Not an error").stack
      }# }}};\n`;
    } catch (err) {
      alert("Blackbox full! Clear me!");
    }
  }
};

export default blackBox;
